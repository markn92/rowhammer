andl $~31, %eax  // Truncate address to 32 bits and mask to be 32-byte-aligned.
addq %r15, %rax  // Add %r15, the sandbox base address.
jmp *%rax  // Indirect jump.
