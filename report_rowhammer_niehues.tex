\input{src/header}

\begin{document}
 
\title{Row Hammer}
\subtitle{Proseminar Praktische IT-Sicherheit WS 18/19\\
Tudor Soroceanu, Benjamin Zengin}
\author{Mark Niehues}

\maketitle
\section{Einleitung}

Sicherheitsprobleme existieren auf verschiedensten Abstraktionsebenen eines Computersystems. Üblicherweise verlassen sich höher liegende Ebenen auf die korrekte Funktionsweise der darunterliegenden Schichten. Dementsprechend führt eine Sicherheitslücke in einer grundlegenden Abstraktionsebene dazu, dass nicht nur die ungewollte Arbeitsweise selbst ein Problem darstellt, sondern es auch viele davon abhängige Elemente auf höherer Ebene gibt, die durch diese Fehlfunktion kompromittiert werden. 

Bei \enquote{row hammer} handelt es sich um eine Sicherheitslücke, die eng mit der Bauweise üblicher Arbeitsspeichermodule (\textsc{dram}) verbunden ist. Dort wird ein Bit als Ladungszustand eines Kondensators gespeichert. Die Baugröße bringt es nun mit sich, dass benachbarte Speicherzellen elektromagnetisch miteinander wechselwirken und so Entladevorgänge herbeiführen können. Dies bedeutet, dass der als Ladungszustand kodierte Speicherinhalt unbemerkt verändert wird.

Dadurch wird das grundlegende Prinzip des Speicherschutzes kompromittiert. Dieses stellt üblicherweise sicher, dass ein Programm nur auf den ihm zugeordneten Speicherbereich zugreifen kann. Hiermit ist gewährleistet, dass ein Prozess nicht gewollt oder ungewollt andere Prozesse manipuliert. Das erhöht nicht nur die Sicherheit sondern auch die Stabilität eines Systems enorm.

Dabei verlässt man sich jedoch in der Regel darauf, dass der Arbeitsspeicher wie gewohnt funktioniert. Dies bedeutet, dass Lese- und Schreibvorgänge nur an den Adressen, an die sie adressiert sind, wirken. Ist dies auf unterster Ebene nicht sichergestellt, sind die Mechanismen, die auf höherer Ebene den Speicherzugriff regulieren, ausgehöhlt. Die in dieser Ausarbeitung präsentierte Sicherheitslücke kompromittiert genau auf diese Art und Weise den Arbeitsspeicher und ermöglicht daher theoretisch die Umgehung des Speicherschutzes. Für einen tatsächlichen Angriff müssen allerdings gewissen Kenntnisse, zum Teil über die Art des Speicherschutzes selbst, bekannt sein, sodass das Gefahrenpotential relativiert wird.

Das eigentliche Problem, dass die Speicherzellen eines Arbeitsspeichers nicht vollständig frei von gegenseitigen Wechselwirkungen sind, ist lange bekannt. Bereits bei der Entwicklung des ersten kommerziellen \textsc{dram} Arbeitsspeicherchips im Jahre 1970 \cite{wiki:intel} war sich Intel dieses Problems bewusst \cite{wiki:rowhammer}.

In dem 2014 veröffentlichten Paper \enquote{Flipping Bits in Memory Without Accessing Them: An Experimental Study of DRAM Disturbance Errors} \cite{kim} wurde gezeigt, dass die Gegenmaßnahmen der Hersteller nicht ausreichend sind. Bei der Untersuchung derzeit aktueller, kommerziell verfügbarer, Arbeitsspeichermodule hat sich gezeigt, dass fehlerhaftes Verhalten in vielen Fällen provoziert werden konnte.

Im Folgenden wird zunächst auf den allgemeinen Aufbau eines Arbeitsspeichers eingegangen. Dies ermöglicht anschließend, das Sicherheitsproblem und deren Ausnutzung nachzuvollziehen. Da die Vorgehensweise sehr nah am Bauteil selbst erklärt wird, folgt im dritten Kapitel ein Beispiel: Anhand einer 2015 veröffentlichten Sicherheitslücke \cite{wiki:rowhammer} im Browser Chrome wird demonstriert, wie ein unzuverlässig arbeitendes Speichermodul für einen Exploit auf Anwendungsebene ausgenutzt werden kann.

Anschließend werden einige Gegenmaßnahmen präsentiert und deren Wirksamkeit abgewogen. Daraus ergibt sich schließlich im letzten Kapitel eine zusammenfassende Einschätzung des Sicherheitsrisikos.

\section{Technische Grundlagen}
Für das Verständnis der Sicherheitslücke ist es wichtig, den technischen Aufbau eines Arbeitsspeichermoduls zu kennen. Ein Arbeitsspeicher besteht üblicherweise aus mehreren so genannten \textit{Speicherbänken}. Ein \textsc{ddr-3-sdram} besteht beispielsweise aus 8 Speicherbänken \cite{wiki:dram}. Eine Speicherbank ist die kleinste Einheit eines Moduls, welche über Adressen gesteuert Daten lesen und schreiben kann.

In Abbildung \ref{fig:bank} ist eine Speicherbank schematisch dargestellt. Die Speicherzellen sind in einer zweidimensionalen Matrix aus Spalten und Reihen angeordnet. Um ein Datum aus dem Arbeitsspeicher auszulesen, wird zunächst die Adresse der Reihe auf den Adressbus als Spannung angelegt. Die entsprechende Reihe in der Bank wird anschließend \textit{aktiviert}. Dies bedeutet, dass entlang der gesamten Reihe eine Spannung angelegt wird, welche dazu führt, dass die Speicherzellen der Reihe ihre Spannung wiederum entlang der Zeilenverbindung in den sog. Leseverstärker abgeben. Aus dem Leseverstärker wird nun mithilfe der Spaltenadresse eine oder mehrere Spalten (sprich Bits) aktiviert und damit eine Verbindung zur Datenleitung hergestellt. \cite{wiki:dram}

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
	\includegraphics[width=\textwidth]{img/dram}
    		 \caption{Im nicht aktivierten Zustand.}
     \label{fig:bank}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.45\textwidth}
\includegraphics[width=\textwidth]{img/dram_hammered}
     \caption{Mit einer ausgewählten Zielreihe (lila) und zwei benachbarten Reihen (gelb), welche abwechselnd aktiviert werden um Ladungsverluste in der Zielreihe zu provozieren.}
     \label{fig:bank-hammered}
\end{subfigure}
\caption{Schematischer Aufbau einer \textsc{dram}-Bank. Links sind Steuerbits (\textbf{RAS}) der Reihenauswahl eingezeichnet. Unten die Steuerbits der Spaltenauswahl (\textbf{CAS}). Diese Steuerbits signalisieren, ob die am Adressbus (\textbf{A}) anliegende Adresse einer Spalte oder Reihe gilt. Die Steuerbits, die als \textbf{Daten} in der Abbildung gekennzeichnet sind, legen fest, ob schreibend oder lesend auf dem rot eingezeichneten Leseverstärker gearbeitet wird. Quelle: \cite{wiki:rowhammer}}
\end{figure}

Beim Aktivieren einer Reihe kommt es zu Spannungsschwankungen entlang der die Speicherzellen (einer Reihe) verbindenden Leitung. Diese Spannungsschwankungen scheinen Ursächlich zu sein für den plötzlichen Ladungsverlust benachbarter Speicherzellen \cite{kim}. Um möglichst viele Entladungen zu provozieren gilt es also, die Anzahl der Reihenaktivierungen zu maximieren.

\section{Vorgehensweise}
Um eine Reihe zu aktivieren reicht es, wie im vorherigen Kapitel erklärt worden ist, aus, auf diese lesend zuzugreifen. Um die Anzahl der Aktivierungen zu maximieren, wird dieser Lesevorgang sehr häufig wiederholt. Dabei reicht es nicht wie in Beispiel \ref{code:rowhammerb} aus, auf \textit{eine} Reihe wiederholt lesend zuzugreifen und den Zwischenspeicher zwischen den Lesevorgängen zu leeren. Denn außer dem eigentlichen Zwischenspeicher, funktioniert auch der Leseverstärker einer Bank als (kleiner) Zwischenspeicher (für die zuletzt gelesene Reihe), sodass mindestens zwei verschiedene Reihen abwechselnd ausgelesen werden müssen. Der entsprechende Assembler-Code ist in Beispiel \ref{code:rowhammera} aufgelistet.

\begin{figure}
\begin{subfigure}[t]{0.45\textwidth}	
	\lstinputlisting[language={[x64]Assembler}]{code/rowhammer_a.asm}
	\caption{}
	\label{code:rowhammera}
\end{subfigure}
\quad
\begin{subfigure}[t]{0.45\textwidth}	
	\lstinputlisting[language={[x64]Assembler}]{code/rowhammer_b.asm}
	\caption{}
	\label{code:rowhammerb}
\end{subfigure}
\caption{Assembler Code der in \cite{kim} verwendet wurde, um Speicherinhalte zu manipulieren. (b) führte zu keinem Fehlverhalten, da bei wiederholtem Auslesen derselben Reihe diese nicht neu aktiviert werden muss, sondern erneut aus dem Leseverstärker gelesen wird. Die \texttt{mfence} Anweisung stellt sicher, dass die Leseanweisungen nicht zur Optimierung intern verschachtelt werden.}
\end{figure}

Entscheidend für die Wirksamkeit dieser Methode ist, dass die ausgewählten Adressen auf \textbf{verschiedene Reihen derselben Bank} zeigen, da jede Bank einen eigenen Leseverstärker (also Zwischenspeicher) hat und die störende Wirkung der Aktivierung einer Reihe nur sehr lokalen Einfluss hat. In \cite{kim} kam es nur bei maximal 2.53 \% der Durchläufe zu Speichermanipulationen in Reihen, die nicht direkt neben den \textit{Aggressor}-Reihen lokalisiert sind. Die meisten Speichermanipulationen in einer sogenannten Zielreihe werden erzeugt, wenn beide ihrer benachbarten Reihen abwechselnd aktiviert werden. 

Abgesehen davon, lassen sich keine Vorhersagen über die entstehenden Änderungen in den Zielreihen treffen. Ob ein Kondensator durch die Aktivierung einer benachbarten Reihe entladen wird, hängt laut \cite{kim} sowohl mit dem eigenen Ladungszustand (der Kondensator muss geladen sein), als auch mit den Ladungszuständen der benachbarten Zellen zusammen. Es ist also ein komplexes n-Körper-Problem, dessen genaue Abhängigkeiten nicht bestimmt werden konnten.

Das Problem ist allerdings sehr verbreitet. Jedes der in \cite{kim} getesteten \textsc{dram}-Module wies ab einem bestimmten Baujahr Fehler auf. Die Korrelation mit dem Baujahr rührt wahrscheinlich daher, dass die verringerte Baugröße, die Isolation zwischen den Zellen erschwert. \cite{kim}

\section{Als Exploit}
Wie im vorherigen Kapitel erwähnt wurde, ist es sehr wahrscheinlich, dass viele auf dem Markt verfügbare \textsc{dram}-Module anfällig für die row-hammer-Methode sind. Um diese Sicherheitslücke in der Praxis auszunutzen müssen allerdings mehrere Hürden genommen werden.

\subsection{Reihen Lokalisieren}
Damit der in Abbildung \ref{code:rowhammera} gezeigte Algorithmus wirksam arbeitet, ist es - wie zuvor erwähnt - wichtig, dass die verwendeten Adressen auf Daten in verschieden Reihen derselben Bank zeigen. In modernen Betriebssystem erfährt ein Prozess allerdings nicht die physikalischen Adressen, welche direkt die Speicherzellen adressieren, sondern arbeitet mit virtuellen Adressen, die von dem Betriebssystem verwaltet und in physikalische Adressen umgerechnet werden. Des weiteren werden die physikalischen Adressen nicht beispielsweise linear auf die Bänke des Moduls verteilt. Um eine möglichst geringe Latenz zu erreichen, werden die Adressen den Bänken so zugeordnet, dass diese gleichmäßig ausgenutzt werden. Wie dies im Detail geschieht ist häufig Betriebsgeheimnis \cite{kim}. Damit es überhaupt zu den kritischen Wechselwirkungen kommt, muss der Angreifer demnach Wissen darüber haben, wie die virtuellen Adressen in physikalische Adressen umgewandelt werden und wo sich diese wiederum tatsächlich auf dem Speichermodul befinden. In der Regel ist es sehr schwer an dieses Wissen zu gelangen. Und selbst wenn, so wäre dieses Wissen wahrscheinlich nur für einen Hersteller, wenn nicht sogar nur für ein Speichermodell gültig. Tatsächlich ist es häufig einfacher, einen großen Speicherbereich zu reservieren und auf diesem zufällig Adresspaare auszuwählen, in der Hoffnung, nahestehende Reihen einer Bank zu aktivieren.

\subsection{Zwischenspeicher umgehen}
Die genannte Methode wird außerdem wirkungslos, sobald die angeforderten Daten nicht mehr direkt aus dem Arbeitsspeicher gelesen werden, da es so nicht zu der Aktivierungen einer Reihe kommt. Dementsprechend muss der Angreifer verhindern, dass die Daten zwischengespeichert werden. Wenn Assembler-Code ausgeführt werden darf, lässt sich dies sehr einfach durch den Befehl \texttt{clflush} (Cachelineflush) sicherstellen. Falls dies nicht möglich ist, gibt es weitere Möglichkeiten die Zwischenspeicher (in der Regel sind es mehrere) zu umgehen. Beispielsweise durch geschickten Einsatz großer Arrays. Dadurch ist es gelungen, den Arbeitsspeicher mittels eines Javascript Programms zu manipulieren\footnote{Weitere Informationen dazu finden sich in \cite{rowhammerjs}.}.

Im Folgenden Kapitel wird ein Angriff vorgestellt, bei dem der row-hammer-Effekt ausgenutzt werden konnte, um Shell-Code in dem Google Chrome Browser auszuführen.

\section{Beispiel: Google NaCl Exploit}
Ein Jahr nach der Arbeit \cite{kim}, die sich vor allem mit der Charakterisierung des Effekts beschäftigte, veröffentlichte ein Team von Google Mitarbeitern (genannt \enquote{Project Zero}) im Jahr 2015 einen funktionierenden Angriff auf den Google Chrome Browser, der sich des row-hammer-Effekts bedient. \cite{projectzero}

Anfällig war dieser über eine Schnittstelle genannt \enquote{Google Native Client} kurz \textsc{nacl}. Diese Schnittstelle ermöglicht es Entwicklern, systemnahen Code in einer Sandbox auszuführen, um rechenintensive Aufgaben im Browser durchführen zu können. Aus diesem Grund erlaubt die Sandbox eine Teilmenge des x86-64 Befehlssatzes. Unter anderem den Befehl \texttt{clflush} zum leeren des Caches. \cite{projectzero}

Außerdem ist es erlaubt, auf den eigenen Programmcode in der Sandbox lesend zuzugreifen. Dies ist sehr nützlich, da es im Weiteren ermöglicht, das an sich zufällige Ergebnis der row-hammer-Methode zu überprüfen.

Grundidee des Angriffes ist, mittels Manipulation des Speichers, Sprung-Befehle so zu verändern, dass diese auf schädliche Anweisungen zeigen. Um die Sicherheit des Browsers zu gewährleisten, wird vor der Ausführung geprüft, ob der Programmcode, der innerhalb der Sandbox ausgeführt werden soll, nur Sprünge zu so genannten 32-Byte ausgerichteten Adressen enthält. Das bedeutet, dass die ersten 31 Bit der Adresse nur aus Nullen bestehen. Damit wird verhindert, dass schädliche Anweisungen in harmlosen Anweisungen versteckt werden können (siehe Abbildung \ref{code:mal}). Wenn es durch Manipulation des Speicherbereichs, in dem der Programmcode abgelegt ist, gelingt, die Zieladressen der validierten Sprunganweisungen zu ändern, können solch versteckte, schädliche Anweisungen dennoch adressiert und ausgeführt werden. Eine beispielhaft genannte Sprunganweisunge, die manipuliert werden kann ist in Abbildung \ref{code:jump} gelistet. \cite{projectzero}

\begin{figure}
	\lstinputlisting[language={[x64]Assembler}]{code/jump.asm}
	\caption{Eine beispielhafte (indirekte) Sprung-Anweisung, wie sie in dem veröffentlichten Angriff verwendet wurde. Da nur die im \texttt{rax}-Register hinterlegte Adresse überprüft wird, kann diese Anweisung kompromittiert werden, wenn durch häufige Lesezugriffe auf den Speicherbereich, dass Registerbit verändert wird und die Adresse anschließend beispielsweise aus dem \texttt{rcx}-Register gelesen wird. Quelle: \cite{projectzero}}
	\label{code:jump}
\end{figure}

\begin{figure}
	\lstinputlisting[language={[x64]Assembler}]{code/schadcode.asm}
	\caption{Innerhalb eines harmlosen Befehls ist eine Syscall-Anweisung versteckt. Quelle: \cite{projectzero}}
	\label{code:mal}
\end{figure}

Zunächst muss allerdings auch hier das Problem der Lokalisierung der Reihen gelöst werden. Wie erwähnt hat ein Prozess - und somit auch der Programmcode der in der Sandbox ausgeführt wird - nur Zugriff auf virtuelle Adressen. Diese geben keinen Aufschluss über den physikalischen Standort der Speicherzelle. 

Nützlich ist an dieser Stelle, dass es in der Sandbox möglich ist, einen großen Speicherbereich zu reservieren. Statt analytisch die Übersetzung der virtuellen Adressen auf die Speicherbänke zu untersuchen, werden zufällige Adresspaare für zwei Reihen geraten. Zusätzlich wird die zu manipulierende Anweisung sehr häufig in den reservierten Speicherbereich kopiert, um die Wahrscheinlichkeit zu erhöhen, an der richtigen Stelle eine Störung hervorzurufen.

Um die Effizienz des Angriffes weiter zu steigern, werden die Abstände der Adresspaare variiert und deren Effektivität statistisch untersucht. Der stärkste Effekt wird erzielt, wenn der Abstand zwischen den Adresspaaren genau dem Abstand zweier Reihen entspricht, da so die Zielreihe von zwei Seiten gestört wird.

Ist dieser relative Abstand zweier Reihen bekannt, lässt sich der Speicher mit hoher Wahrscheinlichkeit manipulieren. Zwar ist die Verteilung der Adressen auf die Speicherbänke weiterhin unbekannt, jedoch besitzt beispielsweise ein \textsc{ddr3}-Speichermodul 8 Speicherbänke. Die Wahrscheinlichkeit zufällig zwei Adressen derselben Bank auszuwählen beträgt also $\frac{1}{8}$.

Insgesamt ist es dem Forscherteam gelungen, 13 \% der provozierten, zufällig auftretenden Speicherfehler auszunutzen, um nicht genehmigte Befehle auszuführen. Als Gegenmaßnahme verbaten die Entwickler des Native Clients den Befehl zum leeren des Caches innerhalb der Sandbox \cite{projectzero}. Dadurch wird der Angriff wirkungslos. 

Diese Maßnahme ist natürlich nur in derartigen virtuellen Umgebungen überhaupt möglich. Da diese Angriffe auf mangelhafter Isolierung der Speicherzellen beruhen, wäre eine naheliegende Möglichkeit, diese Angriffe zu verhindern, die Isolierung zu verbessern. Dies ist laut \cite{kim} aber unrealistisch, da Hersteller ihre Kapazitäten darauf konzentrieren, neue Modelle mit kleineren Baugrößen herzustellen und die Qualität der Isolierung aus wirtschaftlichen Gründen zweitrangig ist. Allerdings gibt es weitere Ansätze, mit denen das Problem eingedämmt werden kann. Diese werden im folgenden Kapitel genannt.

\section{Gegenmaßnahmen}
In \cite{kim} werden einige Möglichkeiten zur Eindämmung der Sicherheitslücke genannt. 

\subsection{Fehlerkorrektur}
Sogenannte \textsc{ECC}-Module erkennen fehlerhafte Datenzustände im Speicher und bessern diese wieder aus. Diese Module sind teurer als normale Speichermodule und werden dementsprechend vor allem in sicherheitskritischen Systemen verwendet. Es wurde allerdings gezeigt, dass diese Module versagen, sobald mehr als zwei Speicherzellen zugleich ihren Zustand ändern \cite{ecc}.

\subsection{Höhere Erneuerungsrate}
Da Kondensatoren immer einen gewissen Ladungsverlust besitzen, führen Prozessoren in regelmäßigen Abständen einen \enquote{Refresh} durch. Dabei wird eine Reihe ausgelesen  und wieder zurückgeschrieben, um den Ladungszustand der Kondensatoren aufzufrischen. Wenn man diesen Vorgang häufiger durchführt, ist der Angreifer nicht mehr in der Lage, einen ausreichenden Ladungsverlust herbeizuführen, um den Speicherzustand zu manipulieren \cite{kim}. Allerdings hängt das Aktualisieren des Speicherzustands gezwungenermaßen mit einer Verringerung der Rechenleistung zusammen. Bei den in \cite{kim} Untersuchten Speicherchips würde ausreichender Schutz mit einer Leistungseinbuße von bis zu 35 \% einhergehen. Dies würde die Nutzbarkeit eines Systems sehr einschränken.

\subsection{Gefährdete Reihen Identifizieren}
Eine weitere Idee besteht daher darin, \enquote{gefährdete} Reihen gezielt zu identifizieren und nur diese häufiger zu Aktualisieren. Der Algorithmus zum Erkennen der gefährdeten Reihen ist für die Effizeinz dieser Methode kritisch. Als mögliche Lösung wurde in \cite{kim} eine Algorithmus präsentiert, der nur mit einer zuvor festgelegten Wahrscheinlichkeit beim Aktivieren einer Reihe, deren benachbarte (also gefährdete) Reihen aktualisiert. Dadurch ist die Zahl der zusätzlichen Zugriffe weniger groß, als in der zuvor genannten Methode. Wenn allerdings eine Reihe sehr oft aktiviert wird, so ist auch die Wahrscheinlichkeit hoch, dass deren benachbarte Reihen häufiger aktualisiert und somit vor Manipulation geschützt wird. Da dies effizienter ist, als die Erneuerungsrate generell für alle Reihen einer Speicherbank zu erhöhen, werden ähnliche Varianten von modernen Mikroarchitekturen unterstützt \cite{wiki:rowhammer}.

\section{Fazit}
Die vorgestellte Sicherheitslücke ist besonders, da sie eine sehr physikalische Ursache hat, und auf sehr viel höherer Abstraktionsebene ausgenutzt werden kann. Es zeigt sich jedoch, dass auch wenn der Arbeitsspeicher in vielen Fällen anfällig ist, ein gezielter Angriff nur unter sehr bestimmten Umständen durchgeführt werden kann. Das liegt zum einen an der Abstraktion des physikalischen Speichers und zum anderen an den mehreren Zwischenspeichern die zur Optimierung der Rechenleistung zwischen CPU und Speicher verbaut werden. Zusätzlich erschwert die zufällige Natur der produzierten Störungen im Speicher die gezielte Ausnutzung.

Aufgrund dieser Schwierigkeiten sind vermutlich wenige funktionierende Angriffe auf Anwenderschicht bekannt. Nichtsdestotrotz ist es ein gravierendes, latentes Sicherheitsproblem, da es ein grundlegendes Integritätskriterium außer Kraft setzt, indem Speichermanipulationen nicht nur an den Stellen wirken, an denen sie adressiert sind. Zusätzlich wird die Isolation der Speicherzellen mit Verringerung der Baugröße erschwert, daher ist es wahrscheinlich, dass diese oder ähnliche Sicherheitslücken ein wiederkehrendes Problem sein werden.

\printbibliography %hier Bibliographie ausgeben lassen
\end{document}      
